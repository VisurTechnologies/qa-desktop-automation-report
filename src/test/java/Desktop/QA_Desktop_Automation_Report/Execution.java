package Desktop.QA_Desktop_Automation_Report;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Execution {

	@Test
	public void test() throws Exception {

		Connection con;
		
		String s1 = null,s2 = null,startDate,endDate;

		String Driver_Class = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		String DBURL = "jdbc:sqlserver://visur.database.windows.net;" + "databaseName=ReportDB;";
		String DBUserName = "visuradmin";
		String DBPassword = "B@*v]c6q_JY]V:H[";

		Class.forName(Driver_Class);
		con = DriverManager.getConnection(DBURL,DBUserName,DBPassword);
		con.createStatement();
		System.out.println("connection established");


		File file = new File("C:\\workspace\\QA-AutomationDesktopExecution-Frontend-build-FDC\\desktop-automation\\Testing-Report.xml");  

		System.out.println(file);

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();  

		DocumentBuilder db = dbf.newDocumentBuilder();  
		Document doc = db.parse(file);  
		doc.getDocumentElement().normalize();  

		System.out.println("Root element: " + doc.getDocumentElement().getNodeName());  


		NodeList nodeList = doc.getElementsByTagName("Result");  


		for (int itr = 0; itr < nodeList.getLength(); itr++)   
		{  
			Node node = nodeList.item(itr);  
			System.out.println("\n");  

			if (node.getNodeType() == Node.ELEMENT_NODE)   
			{  

				Element eElement = (Element) node;  


				String TestScenario = eElement.getElementsByTagName("TestScenario").item(0).getTextContent();
				String TestName = eElement.getElementsByTagName("TestName").item(0).getTextContent();
				String Started = eElement.getElementsByTagName("Started").item(0).getTextContent();
				String Ended = eElement.getElementsByTagName("Ended").item(0).getTextContent();
				String Status = eElement.getElementsByTagName("Status").item(0).getTextContent();

				System.out.println(TestScenario);  
				System.out.println(TestName);  
				System.out.println(Status);  
				
				String str1 = Started; 
				String[] arrOfStr1 = str1.split(" "); 

				for (String a : arrOfStr1) {
					s1 = a;
				} 
				
				startDate = s1;
				

				/////////////////////////////////////////////////////////////////////////
				
				String str2 = Ended; 
				String[] arrOfStr2 = str2.split(" "); 

				for (String a : arrOfStr2) {
					s2 = a;
				} 
				
				endDate = s2;
				
				
				
				String time1 = startDate;
				String time2 = endDate;
		 
				SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
				Date date1 = format.parse(time1);
				Date date2 = format.parse(time2);
				long difference = date2.getTime() - date1.getTime();
				
				long TimeToExecute_Minutes = (difference/1000)/60;
				long TimeToExecute_Seconds = difference/1000;
				
				System.out.println(TimeToExecute_Minutes);
				System.out.println(TimeToExecute_Seconds);
				
				
//				System.out.println(TimeUnit.MILLISECONDS.toMinutes(difference));	
				
				/////////exact minits with millsec
//		        long p2 = difference / 60;
//                System.out.println("p2="+p2);





				String sr = "Insert into [dbo].[QA_DeskTop_Automation_Report]([TestScenario],[TestName],[TimeToExecute_Minutes],[TimeToExecute_Seconds],[Status]) values(?,?,?,?,?)";
				PreparedStatement st = con.prepareStatement(sr,Statement.RETURN_GENERATED_KEYS);

				st.setString(1, TestScenario);
				st.setString(2, TestName);
				st.setDouble(3, TimeToExecute_Minutes);
				st.setDouble(4, TimeToExecute_Seconds);
				st.setString(5, Status);
				int c = st.executeUpdate();
				System.out.println(c + " record saved");

			}  

			
		}  

		con.close();
		System.out.println("connection closed");
	}
	
	

}
